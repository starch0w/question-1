class Calculator
  defined_numbers = {
    zero: 0,
    one: 1,
    two: 2,
    three: 3,
    four: 4,
    five: 5,
    six: 6,
    seven: 7,
    eight: 8,
    nine: 9,
    ten: 10
  }
  defined_operators = {
    plus: :+,
    minus: :-,
    times: :*,
    divided_by: :/
  }

  defined_numbers.each do |number_in_word, number|
    define_method(number_in_word) do |args=nil|
      return number unless args
      # p "=====+#{number}, #{args} "
      number.send(args[0], args[1])
    end
  end

  defined_operators.each do |string_operator, operator|
    define_method(string_operator) do |n|
      [operator, n]
    end
  end
end