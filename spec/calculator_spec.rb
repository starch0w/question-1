require './calculator'
require 'humanize'

RSpec.describe Calculator do
  describe "#numbers" do
    it "return itself if there no parameter" do
      calculator = Calculator.new
      0..10.times do |i|
        expect(calculator.send(i.humanize)).to eq(i)
      end
    end

    it "return value of math" do
      calculator = Calculator.new
      operators = {plus: :+, minus: :-, times: :*, divided_by: :/}

      0..10.times do |i|
        operators.each do |operator_word, operator|
          n = Random.rand 100
          expect(calculator.send(i.humanize, calculator.send(operator_word, n))).to eq(i.send(operator, n))
        end
      end
    end
  end
end